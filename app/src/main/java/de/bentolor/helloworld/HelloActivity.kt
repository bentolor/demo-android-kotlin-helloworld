package de.bentolor.helloworld

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_hello.*
import java.util.*

class HelloActivity : AppCompatActivity() {

    private val gast = Gast(name = "Publikum")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        modelToView()

        editName.setOnEditorActionListener { v, i, e ->
            gast.name = editName.inhalt()
            modelToView()
            false
        }
        anredeGroup.setOnCheckedChangeListener { g, id ->
            gast.titel = when (id) {
                radioHerr.id -> Titel.Herr
                radioFrau.id -> Titel.Frau
                else -> Titel.wertes
            }
            modelToView()
        }
    }

    fun modelToView() {
        labelGruss.text = gast.anrede
    }
}

// Erweiterung einer Android-Klasse
fun EditText.inhalt(): String = text.toString()

enum class Titel { wertes, Herr, Frau, Doktor }
data class Gast(var titel: Titel = Titel.wertes,
                var name: String, val geburt: Date? = null) {

    val anrede: String
        get() = "Hallo $titel $name!"

    // + automatisch: equals(), hashCode(), toString(), copy(), …
}